#ifndef EXTENSOMETER_H
#define EXTENSOMETER_H
#include <vector>
#include <QtGui/QMainWindow>
#include <QImage>
#include <QLabel>
#include <QPixmap>
#include <QTableWidget>
#include <QMessageBox>
#include <QSignalMapper>
#include "opencv/highgui.h"
#include "opencv/cv.h"
#include "opencv/cvaux.h"
#include "ui_extensometer.h"
#include "display.h"



using namespace cv;

//!Główna klasa programu - odpowiada za GUI, uruchamianie odpowiednich obliczeń, a także stereokalibrację i triangulację
class Extensometer : public QMainWindow
{
	Q_OBJECT

public:
	Extensometer(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Extensometer();
	
	public slots:
		//! pobiera pozycje markerów w poszczególnych scenach, liczy stereokalibrację, triangulację, przekazuje do Display
		void analyse();
		void reset();
	

private:
	Ui::ExtensometerClass ui;
	
protected:

	//! metoda triangulacji - nieuzywana w obecnej wersji
	CvPoint3D64f triangulateIT( CvPoint2D64f pixel1,  CvPoint2D64f pixel2, Mat &R1, Mat &R2, Mat &T1, Mat &T2, Mat &cam1, Mat &cam2, Mat &dist1, Mat &dist2, Mat &camProj1, Mat &cam2Proj);
	//! pomocnicza do triangulacji, nieużywana
	void solveDLTEigen(cv::Mat &X,  CvPoint2D64f pixel1,  CvPoint2D64f pixel2, double weight1, double weight2, const Mat &camProj1, const Mat &camProj2);

	QSignalMapper * signalMapper;

	QList<QList<CvPoint3D64f>> results;
	Display * display;




	
};

#endif // EXTENSOMETER_H
