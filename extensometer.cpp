﻿#include "extensometer.h"
#define QMARKERS 1
#define CIRCLEMARKERS 2
Extensometer::Extensometer(QWidget *parent, Qt::WFlags flags)
: QMainWindow(parent, flags)
{


	ui.setupUi(this);

	connect(ui.actionKalibruj,  SIGNAL(triggered()), ui.series1, SLOT(calibrate()));
	connect(ui.actionWczytaj,  SIGNAL(triggered()), ui.series1, SLOT(calibrateFromFile()));
	connect(ui.loadPhotos1Button, SIGNAL(clicked()), ui.series1, SLOT(loadPhotos()));
	connect(ui.reset1Button, SIGNAL(clicked()), ui.series1, SLOT(clear()));

	connect(ui.actionKalibruj2,  SIGNAL(triggered()), ui.series2, SLOT(calibrate()));
	connect(ui.actionWczytaj2,  SIGNAL(triggered()), ui.series2, SLOT(calibrateFromFile()));
	connect(ui.loadPhotos2Button, SIGNAL(clicked()), ui.series2, SLOT(loadPhotos()));
	connect(ui.reset2Button, SIGNAL(clicked()), ui.series2, SLOT(clear()));

	connect(ui.analyseButton, SIGNAL(clicked()), this, SLOT(analyse()));

	display = NULL;
	setWindowTitle(tr("Ekstensometr"));


}

Extensometer::~Extensometer()
{

}
void Extensometer::reset()
{
	ui.series1->clear();

	//tables.clear();
	//pointsVect.clear();


	ui.markersDeclared->setEnabled(1);
	ui.circleMarkersButton->setEnabled(1);
	ui.qMarkersButton->setEnabled(1);
	return;
}

void Extensometer::analyse()
{

	int markerType;
	if(ui.circleMarkersButton->isChecked())
		markerType = CIRCLEMARKERS;
	if(ui.qMarkersButton->isChecked())
		markerType = QMARKERS;


	//uzywana jest albo tylko seria 1, albo obie. 
	//jesli nie dodano zdjec - nic sie nie dzieje
	// jesli jest tylko 1 seria - trzecia wspolrzedna jest ustalona na 1
	if(ui.series1->count() == 0)
		return;

	int pointsInPicture = ui.markersDeclared->value();
	int picturesCount = ui.series1->count();


	//jesli zdjecia sa wylacznie w serii 1 - "tlumaczenie" na punkty 3D
	if(ui.series1->count() > 0 && ui.series2->count() == 0)
	{
		//indeksowanie  [numerPunktu][numerZdjecia]
		QList<CvPoint2D32f> empty;
		QList<QList<CvPoint2D32f>> results1 = ui.series1->getPositions(markerType, ui.markersDeclared->value(), empty); //kolejność obojętna - pusta lista jako reference
		
		Mat points1Mat; //wspolrzedne punktow - poczatkowe
		Mat points1NormMat; //wspolrzedne po korekcji dystorsaji i normalizacji

		points1Mat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
		
		//przeniesienie do formatu openCV
		for(int i=0; i< pointsInPicture; i++){
			for(int j=0; j < picturesCount; j++) //najpierw wszystkie polozenia danego punktu, potem nastepny punkt
			{
				points1Mat.at<Vec2d>(0, i*picturesCount+j )[0] = (double) results1[i][j].x;
				points1Mat.at<Vec2d>(0, i*picturesCount+j )[1] = (double) results1[i][j].y;
			}
		}
		//korekcja dystorsji - jeśli możliwa
		
		if(ui.series1->calibrated)
		{
			points1NormMat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
			Mat intrinsic1 = cvarrToMat(ui.series1->calibrator->intrinsicMatrix, true);
			Mat distortion1 = cvarrToMat(ui.series1->calibrator->distortionCoeffs, true);
			undistortPoints(points1Mat, points1NormMat, intrinsic1, distortion1);
			double fx = intrinsic1.at<float>(0, 0);
			double fy = intrinsic1.at<float>(1, 1);
			for(int i=0; i< pointsInPicture*picturesCount; i++)//skalowanie * ogniskowa, aby dostać współrzędne pikselowe
			{
				points1NormMat.at<Vec2d>(i)[0]*=fx; 
				points1NormMat.at<Vec2d>(i)[1]*=fy;

			}

		}else{
			points1NormMat = Mat(points1Mat);
		}

		
		

		results.clear();

		for(int i=0; i< pointsInPicture; i++)
		{
			QList<CvPoint3D64f> newCurrentPointPositions;
			for(int j=0; j < picturesCount; j++) //najpierw wszystkie polozenia danego punktu, potem nastepny punkt
			{
				CvPoint3D64f newCurrentPosition;
				newCurrentPosition.x = points1NormMat.at<Vec2d>(i*picturesCount + j)[0]; 
				newCurrentPosition.y = points1NormMat.at<Vec2d>(i*picturesCount + j)[1];
				newCurrentPosition.z = 1;
				newCurrentPointPositions.append(newCurrentPosition);
			}
			results.append(newCurrentPointPositions);
		}
	}else{
		//jesli sa zdjecia z 2 serii trzeba wykonac stereokalibracje i triangulacje

		if(!ui.series1->calibrated || !ui.series2->calibrated)
		{
			QMessageBox::warning(NULL, "Blad", "Konieczna jest kalibracja obu kamer za pomocą serii odpowiadających sobie zdjęć stereowizyjnych");
			return;
		}else if(ui.series1->count() != ui.series2->count()){
			QMessageBox::warning(NULL, "Blad", "Niezgodna ilość zdjęć z kamer");
		}
		double reprojectionError;
		CvMat * R = cvCreateMat(3, 3, CV_64F);
		CvMat * T = cvCreateMat(3, 1, CV_64F);
		CvMat * E = cvCreateMat(3, 3, CV_64F);
		CvMat * F = cvCreateMat(3, 3, CV_64F);

		reprojectionError = cvStereoCalibrate(ui.series1->calibrator->objectPoints, 
			ui.series1->calibrator->imagePoints, ui.series2->calibrator->imagePoints, 
			ui.series1->calibrator->pointsCounts, 
			ui.series1->calibrator->intrinsicMatrix, ui.series1->calibrator->distortionCoeffs, 
			ui.series2->calibrator->intrinsicMatrix, ui.series2->calibrator->distortionCoeffs, 
			ui.series1->calibrator->imageSize, R, T, E, F, 
			cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),CV_CALIB_FIX_INTRINSIC);

		//indeksowanie  [numerPunktu][numerZdjecia]
		QList<CvPoint2D32f> empty;
		QList<QList<CvPoint2D32f>> results1 = ui.series1->getPositions(markerType, ui.markersDeclared->value(), empty); //kolejność obojętna - pusta lista jako reference
		QList<QList<CvPoint2D32f>> results2 = ui.series2->getPositions(markerType, ui.markersDeclared->value(), ((Scene*)ui.series1->item(0))->positions); //kolejność taka, jak w 1. serii


		//przygotowanie do triangulacji
		//macierz projekcyjna pierwszej kamery 
		double cam1Ext_a[12] = {    1,0,0,    0,
			0,1,0,    0,
			0,0,1,    0};
		Mat cam1Ext = Mat(3,4,CV_64F,cam1Ext_a);

		//macierz projekcyjna kamera 2 [R][T]
		Mat cam2Ext = Mat(3,4,CV_64F);
		for(int i=0; i<3; i++)
		{
			for (int j=0; j<3; j++)
				cam2Ext.at<double>(i, j) = CV_MAT_ELEM(*R, double, i, j);
			cam2Ext.at<double>(i, 3) = CV_MAT_ELEM(*T, double, i, 0);
		}

		Mat points1Mat; //wspolrzedne punktow - poczatkowe
		Mat points2Mat;
		Mat points1NormMat; //tymczasowe wspolrzedne po normalizacji (undistort)
		Mat points2NormMat;
		Mat points1Corr; //po correctMatches
		Mat points2Corr;
		Mat points1Triang;	//format do triangulacji
		Mat points2Triang;

		//CvMat * points1NormMat; //[po normalizacji (undistort)
		//CvMat * points2NormMat;



		points1Mat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
		points2Mat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
		//points1Mat = cvCreateMat(picturesCount*pointsInPicture, 1, CV_64FC2);
		//points2Mat = cvCreateMat(picturesCount*pointsInPicture, 1, CV_64FC2);

		///////////////////////
		/// DEBUG
		/////
		//QString fileName = QFileDialog::getSaveFileName(this, "Zapisz plik", "", ".xml");
		//if(fileName.isEmpty())
		//	return;
		//FileStorage file(fileName.toStdString(), FileStorage::WRITE);

		for(int i=0; i< pointsInPicture; i++)
		{
			for(int j=0; j < picturesCount; j++) //najpierw wszystkie polozenia danego punktu, potem nastepny punkt
			{
				points1Mat.at<Vec2d>(0, i*picturesCount+j )[0] = (double) results1[i][j].x;
				points1Mat.at<Vec2d>(0, i*picturesCount+j )[1] = (double) results1[i][j].y;
				points2Mat.at<Vec2d>(0, i*picturesCount+j )[0] = (double) results2[i][j].x;
				points2Mat.at<Vec2d>(0, i*picturesCount+j )[1] = (double) results2[i][j].y;

			}
		}
		//file << "R" << R;
		//file << "T" << T;
		//file << "E" << E;
		//file << "F" << F;



		Mat intrinsic1 = cvarrToMat(ui.series1->calibrator->intrinsicMatrix, true);
		Mat intrinsic2 = cvarrToMat(ui.series2->calibrator->intrinsicMatrix, true);
		Mat distortion1 = cvarrToMat(ui.series1->calibrator->distortionCoeffs, true);
		Mat distortion2 = cvarrToMat(ui.series2->calibrator->distortionCoeffs, true);

		Mat F_cpp = cvarrToMat(F, true);
		Mat E_cpp = cvarrToMat(E, true);
		Mat distortionZero = Mat::zeros(5,1,CV_64FC1);

		Mat R_cpp = cvarrToMat(R, true);
		Mat T_cpp = cvarrToMat(T, true);

		//STEREORECTIFY - inna opcja, ostatecznie nieuzywana
		/*Mat R1, R2, Q;

		cv::Size imageSize(ui.series1->calibrator->imageSize);
		stereoRectify(intrinsic1, distortion1,
		intrinsic2, distortion2, 
		imageSize, 
		R_cpp, T_cpp, 
		R1, R2, cam1Ext, cam2Ext, Q, CALIB_ZERO_DISPARITY);*/


		


		//file << "points1Corr" << points1Corr;
		//file << "points2Corr" << points2Corr;


		points1NormMat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
		points2NormMat = Mat(1, picturesCount*pointsInPicture, CV_64FC2);
		undistortPoints(points1Mat, points1NormMat, intrinsic1, distortion1);
		undistortPoints(points2Mat, points2NormMat, intrinsic2, distortion2);


		//TRIANGUULATEPOINTS
		correctMatches(E_cpp, points1NormMat, points2NormMat, points1Corr, points2Corr);
		//file << "points1NormMat" << points1NormMat;
		//file << "points2NormMat" << points2NormMat;

		points1Triang = Mat(2, picturesCount*pointsInPicture, CV_64FC1);
		points2Triang = Mat(2, picturesCount*pointsInPicture, CV_64FC1);
		for(int i=0; i<picturesCount*pointsInPicture; i++)
		{
			points1Triang.at<double>(0, i) = points1Corr.at<Vec2d>(0, i)[0];
			points1Triang.at<double>(1, i) = points1Corr.at<Vec2d>(0, i)[1];
			points2Triang.at<double>(0, i) = points2Corr.at<Vec2d>(0, i)[0];
			points2Triang.at<double>(1, i) = points2Corr.at<Vec2d>(0, i)[1];
		}


		Mat points4d(4, picturesCount*pointsInPicture,  CV_64FC1);
		triangulatePoints(cam1Ext, cam2Ext, points1Triang, points2Triang, points4d);
		




		Mat R1 = Mat::eye(3, 3, CV_64FC1); //R dla lewej kamery
		// R - dla prawej - z kalibracji
		Mat T1 = Mat::zeros(3, 1, CV_64FC1); //dla lewej kamery
		// T - dla prawej
		Mat idealCam = Mat::eye(3, 3, CV_64FC1); //dla obukamer
		// distortionZero


		
//		file.release();
		results.clear();
		for(int i=0; i< pointsInPicture; i++)
		{
			QList<CvPoint3D64f> newPointPositions;
			for(int j=0; j < picturesCount; j++) //najpierw wszystkie polozenia danego punktu, potem nastepny punkt
			{
				CvPoint3D64f newPosition;
				double lastCoord = points4d.at<double>(3, i*picturesCount+j);

				newPosition.x = points4d.at<double>(0, i*picturesCount+j)/lastCoord;

				newPosition.y = points4d.at<double>(1, i*picturesCount+j)/lastCoord;

				newPosition.z = points4d.at<double>(2, i*picturesCount+j)/lastCoord;


				//iterative-eigen:
				//CvPoint2D64f a, b; //punkty na obrazach do triangulacji - we wspolrzednych znormalizowanych
				//a = cvPoint2D64f(points1NormMat.at<Vec2d>(i*picturesCount + j)[0], points1NormMat.at<Vec2d>(i*picturesCount + j)[1]);
				//b = cvPoint2D64f(points2NormMat.at<Vec2d>(i*picturesCount + j)[0], points2NormMat.at<Vec2d>(i*picturesCount + j)[1]);
				//newPosition = triangulateIT(a, b, R1, R_cpp, T1, T_cpp, idealCam, idealCam, distortionZero, distortionZero, cam1Ext, cam2Ext);
				newPointPositions.append(newPosition);

			}
			results.append(newPointPositions);
		}
	}
	if(display)
		delete display;
	display = new Display(results, 0);
	display->show();

}
CvPoint3D64f Extensometer::triangulateIT( CvPoint2D64f pixel1,  CvPoint2D64f pixel2, 
										 Mat &R1, Mat &R2, Mat &T1, Mat &T2, 
										 Mat &cam1, Mat &cam2, Mat &dist1, Mat &dist2, Mat &camProj1, Mat &camProj2) 
{
	

	// weights for the equation system: current and new candidates, weight delta (increment/decrement) values
	double w1 = 1.0, w2 = 1.0, w1n, w2n, w1d, w2d;
	// values for epsilon-based termination test for the point distance, and the delta of the weights
	static const double EPS = 1.0e-3, EPSD = 1.0e-8;
	// projections of current X onto cameras 1 and 2
	CvPoint2D64f q1n, q2n;
	// values of projection error (distance of points q1 q2 projected using X from the actual locations of pixel1 and pixel2) and mean
	double err1, err2, errm;


// the current solution
	Mat X;
	solveDLTEigen(X, pixel1, pixel2, w1, w2, camProj1, camProj2);
	//LOG("Starting value for X = " << Point3DD(X));


	// Hartley suggests 10 iterations at most
    for (int i = 0; i < 10; ++i) 
	{
		// calculate the projection of current X onto the cameras (pixel coords), 
		// calculate the distance from the input pixels as the error value
		
		Mat tempX = X( Range(0, 3), Range::all()).clone();
		tempX = tempX.t();




		Mat projected = Mat(2, 1, CV_64FC1);


	
		projectPoints(tempX, R1, T1.t(), cam1, dist1, projected);
		
		q1n = cvPoint2D64f(projected.at<double>(0, 0), projected.at<double>(0, 1));
		projectPoints(tempX, R2, T2.t(), cam2, dist2, projected);
		q2n = cvPoint2D64f(projected.at<double>(0, 0), projected.at<double>(0, 1));
		//q1n = project(X, CAM1);
		//q2n = project(X, CAM2);
		//LOG("Obtained new q1 = (" << q1n << "), q2 = (" << q2n << ")");
		err1 = Display::pointsDistance(pixel1, q1n);
		err2 = Display::pointsDistance(pixel2, q2n);
		errm = (err1 + err2) * 0.5;
		//LOG("Distance/error values: err1 = " << err1 << ", err2 = " << err2 << ", mean = " << errm);

		// if errors are small enough, break
		if (err1 < EPS && err2 < EPS)
		{
			//LOG("Error values satisfy epsilon, breaking");
			//LOGDEC;
			break;
		}
		

		// otherwise calculate set of new weights and their deltas from the old set
		w1n = cv::Mat_<double>(camProj1.row(2) * X)(0);
		w2n = cv::Mat_<double>(camProj2.row(2) * X)(0);
		//LOG("New weights: w1 = " << w1n << ", w2 = " << w2n);
		w1d = fabs(w1n - w1);
		w2d = fabs(w2n - w2);
		//LOG("Weight deltas: w1d = " << w1d << ", w2d = " << w2d);

		// if value change is insignificant, break
		if (w1d < EPSD && w2d < EPSD)
		{
			//LOG("Weights changed value by an insignificant amount in this iteration, breaking");
			//LOGDEC;
			break;
		}

		// otherwise make new weights current, solve reweighted equation system
		w1 = w1n;
		w2 = w2n;
		solveDLTEigen(X, pixel1, pixel2, w1, w2, camProj1, camProj2);
		//LOG("Solved system and obtained X = " << Point3DD(X));
		//LOGDEC;
    }

//	LOGDEC;
    return cvPoint3D64f(X.at<double>(0), X.at<double>(1), X.at<double>(2));
}

void Extensometer::solveDLTEigen(cv::Mat &X,  CvPoint2D64f pixel1,  CvPoint2D64f pixel2, double weight1, double weight2, const Mat &camProj1, const Mat &camProj2) 
{
	//LOGX("Solving DLT system for " << pixel1 << " <--> " << pixel2 << " with w1 = " << weight1 << ", w2 = " << weight2);
	//LOGINC;
	// preallocate matrix of the linear equation system
	cv::Mat A(4, 4, CV_64FC1);
	// calculate rows of A
	cv::Mat 
		A1 = (pixel1.x * camProj1.row(2) - camProj1.row(0)) * (1/weight1),
		A2 = (pixel1.y * camProj1.row(2) - camProj1.row(1)) * (1/weight1),
		A3 = (pixel2.x * camProj2.row(2) - camProj2.row(0)) * (1/weight2),
		A4 = (pixel2.y * camProj2.row(2) - camProj2.row(1)) * (1/weight2);
	// normalizacja rzędów według polecenia typa z SO psuje wyniki, aż do delta > 15px między oryginałem a reprojekcją z triangulacji!
	//A1 /= cv::norm(A1, cv::NORM_L2);
	//A2 /= cv::norm(A2, cv::NORM_L2);
	//A3 /= cv::norm(A3, cv::NORM_L2);
	//A4 /= cv::norm(A4, cv::NORM_L2);
	// compose A from rows
	A1.copyTo(A.row(0));
	A2.copyTo(A.row(1));
	A3.copyTo(A.row(2));
	A4.copyTo(A.row(3));
	//LOG("A: " << A);

	// solve A*X = 0
	cv::SVD::solveZ(A, X);
	// X is in homogeneous coordinates, normalize using last coordinate
	X /= X.at<double>(3);
	//LOG("X: " << X);

	//LOGDEC;
}

