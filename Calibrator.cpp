﻿#include "calibrator.h"

Calibrator::Calibrator(QWidget* parent) : QWidget(parent)
{
	ui.setupUi(this);

	setWindowTitle("Kalibracja");
	connect(ui.resetButton, SIGNAL(clicked()), this, SLOT(reset()));
	connect(ui.saveToFileButton, SIGNAL(clicked()), this, SLOT(saveToFile()));
	connect(ui.calibrateButton, SIGNAL(clicked()), this, SLOT(calibrate()));
	connect(ui.loadPhotosButton, SIGNAL(clicked()), this, SLOT(loadPhotos()));

	calibrated = false;
	intrinsicMatrix = cvCreateMat(3,3,CV_32FC1);
	distortionCoeffs = cvCreateMat(8,1,CV_32FC1);
	cvZero( intrinsicMatrix );
    cvZero( distortionCoeffs );
	imagePoints = NULL;
	objectPoints = NULL;
	photoDeleteShortcut = new QShortcut(QKeySequence(Qt::Key_Delete), ui.photos);
	connect(photoDeleteShortcut, SIGNAL(activated()), this, SLOT(deletePhotos()));
	//this->show();
}

Calibrator::~Calibrator(void)
{
	cvReleaseMat(&intrinsicMatrix);
	cvReleaseMat(&distortionCoeffs);
	if(imagePoints)
		cvReleaseMat(&imagePoints);
	if(objectPoints)
		cvReleaseMat(&objectPoints);
	//if(pointsCounts)
	//	cvReleaseMat(&pointsCounts);
	delete photoDeleteShortcut;
}

void Calibrator::calibrate(){

	boardSize.height = ui.boardHeight->value();
	boardSize.width = ui.boardWidth->value();
	squareSize =ui.boardSquareSize->value();
	imageSize = cvSize(-1, -1);
	bool valid;
	for(int i=0; i < ui.photos->count(); i++)
	{
		ui.photos->scrollToItem(ui.photos->item(i));
		valid = ((CalibrationPhoto*)ui.photos->item(i))->analyse(boardSize);
		CvSize currentImageSize = ((CalibrationPhoto*)ui.photos->item(i))->imageSize;
		if(valid && i==0) //pierwsze poprawne zdjecie ustala obowiazujacy rozmiar
			imageSize = currentImageSize;
		if(!valid || currentImageSize.width != imageSize.width || currentImageSize.height != imageSize.height)
		{ //błędne zdjęcie, do usunięcia
			delete ((CalibrationPhoto*)ui.photos->item(i));
			QMessageBox::warning(0, tr("Błąd"), tr("Wybrano niepoprawny plik"));
			i--;
			continue;
		}
		if(i==0)
			imageSize = ((CalibrationPhoto*)ui.photos->item(0))->imageSize;

	}
	//na liście zostały wyłącznie poprawne zdjęcia kalibracyjne
	
	int imageCount = ui.photos->count();
	int pointCount = boardSize.width*boardSize.height;
	

	if(imageCount < 2){
		QMessageBox::warning(0, tr("Błąd"), tr("Za mało poprawnych plików "));
		return;
	}


	cvZero( intrinsicMatrix );
    cvZero( distortionCoeffs );

	imagePoints = cvCreateMat( 1, imageCount*pointCount, CV_32FC2 );
    objectPoints = cvCreateMat( 1, imageCount*pointCount, CV_32FC3 );
    pointsCounts = cvCreateMat( 1, imageCount, CV_32SC1 );
    CvMat rot_vects, trans_vects;
	// initialize arrays of points
    for(int i = 0; i < ui.photos->count(); i++ )
    {
        CvPoint2D32f* src_img_pt = ((CalibrationPhoto*)ui.photos->item(i))->corners;
        CvPoint2D32f* dst_img_pt = ((CvPoint2D32f*)imagePoints->data.fl) + i*pointCount;
        CvPoint3D32f* obj_pt = ((CvPoint3D32f*)objectPoints->data.fl) + i*pointCount;

        for(int j = 0; j < boardSize.height; j++ )
            for(int k = 0; k < boardSize.width; k++ )
            {
                *obj_pt++ = cvPoint3D32f(j*squareSize, k*squareSize, 0);
                *dst_img_pt++ = *src_img_pt++;
            }
    }
	cvSet( pointsCounts, cvScalar(pointCount) );


	CvMat rotationVectors, translationVectors;
    CvMat *extr_params = cvCreateMat( imageCount, 6, CV_32FC1 );
    cvGetCols( extr_params, &rotationVectors, 0, 3 );
    cvGetCols( extr_params, &translationVectors, 3, 6 );

	/// REPROJECTION


	CvMat *reprojectionErrors = cvCreateMat( 1, imageCount, CV_64FC1 );


	reprojectionError = cvCalibrateCamera2( objectPoints, imagePoints, pointsCounts,
		imageSize, intrinsicMatrix, distortionCoeffs, &rotationVectors, &translationVectors,  0, cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5));
	calibrated = true;
	



	double reprojectionAllCustom = calculateReprojectionError(objectPoints, &rotationVectors, &translationVectors, intrinsicMatrix, distortionCoeffs, 
		imagePoints, pointsCounts, reprojectionErrors);

	QString messageDataLoaded("Kalibracja poprawna. Błąd reprojekcji: ");
	messageDataLoaded.append(QString("%1").arg(reprojectionAllCustom));
	ui.messages->setText(messageDataLoaded);

	for(int i=0; i < ui.photos->count(); i++)
	{

		QString newLabel = ui.photos->item(i)->text();
		newLabel.append(QString::number(CV_MAT_ELEM(*reprojectionErrors, double, 0, i)));
		ui.photos->item(i)->setText(newLabel);
	}
	repaint();

}
void Calibrator::saveToFile(){
	if(!calibrated){
		QMessageBox::warning(this,  "Błąd!", "System nie jest skalibrowany, nie można zapisać");
		return;
	}
	QString fileName = QFileDialog::getSaveFileName(this, "Zapisz plik", "", ".xml");
	if(fileName.isEmpty())
		return;
	CvFileStorage * file = cvOpenFileStorage(fileName.toLatin1(), 0, CV_STORAGE_WRITE);
	int photosCount = ui.photos->count();
	cvWrite(file, "intrinsicMatrix", intrinsicMatrix);
	cvWrite(file, "distortionCoeffs", distortionCoeffs);
	cvWriteInt(file, "boardWidth", boardSize.width);
	cvWriteInt(file, "boardHeight", boardSize.height);
	cvWriteInt(file, "imageWidth", imageSize.width);
	cvWriteInt(file, "imageHeight", imageSize.height);
	cvWriteInt(file, "imageCount", photosCount);
	cvWriteReal(file, "reprojectionError", reprojectionError);
	cvWrite(file, "objectPoints", objectPoints);
	cvWrite(file, "imagePoints", imagePoints);
	cvWrite(file, "pointsCounts", pointsCounts);
	
	cvReleaseFileStorage(&file);




}


void Calibrator::reset(){
	ui.photos->clear();
	calibrated = 0;


	
	cvZero( intrinsicMatrix );
    cvZero( distortionCoeffs );

	if(imagePoints)
		cvReleaseMat(&imagePoints);
	if(objectPoints)
		cvReleaseMat(&objectPoints);
}


void Calibrator::paintEvent ( QPaintEvent * event ){

	ui.frameCounter->setText(QString::number(ui.photos->count()));
	ui.saveToFileButton->setEnabled(calibrated);

}
void Calibrator::closeEvent(QCloseEvent * event)
{
	emit off(calibrated); //0 lub 1
	hide();
}
void Calibrator::loadPhotos(){
	
	QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tiff *.tif *.gif)"));
	if(fileNames.isEmpty()){
		return;
	}
	
	foreach(QString name, fileNames){
		CalibrationPhoto * newPhoto  = new CalibrationPhoto(name, ui.photos);
		ui.photos->addItem(newPhoto);
	}
	


}
bool Calibrator::loadFromFile()
{
	CvFileStorage * file;
	//tymczasowe macierze do wczytywania danych
	
	CvMat * intr; 
	CvMat * dstr; 
	CvMat * imgPts;
	CvMat * objPts;
	CvMat * ptsCts;
	
	QString fileToRead;

		fileToRead = QFileDialog::getOpenFileName(0, "Wybierz plik do wczytania", "", "");
		if(fileToRead.isEmpty())
		{
			QMessageBox::warning(0, "Blad", "Nie wybrano pliku z danymi");
			return 0;
		}

	try{
		file = cvOpenFileStorage(fileToRead.toLatin1(), 0, CV_STORAGE_READ);
		intr = (CvMat*) cvReadByName(file, 0, "intrinsicMatrix");
		dstr = (CvMat*) cvReadByName(file, 0, "distortionCoeffs");
		boardSize.width = cvReadIntByName(file, 0, "boardWidth");
		boardSize.height = cvReadIntByName(file, 0, "boardHeight");
		imageSize.width = cvReadIntByName(file, 0, "imageWidth");
		imageSize.height = cvReadIntByName(file, 0, "imageHeight");
		reprojectionError = cvReadRealByName(file, 0, "reprojectionError");
		objPts = (CvMat*)cvReadByName(file, 0, "objectPoints");
		imgPts = (CvMat*)cvReadByName(file, 0, "imagePoints");
		ptsCts = (CvMat*)cvReadByName(file, 0, "pointsCounts");
	}
	catch(cv::Exception& e){
		return 0;
	}
	cvReleaseFileStorage(&file);

	calibrated=true;
	QString messageDataLoaded("Wczytano dane kalibracyjne, błąd reprojekcji: ");
	messageDataLoaded.append(QString("%1").arg(reprojectionError));
	ui.messages->setText(messageDataLoaded);
	
	imagePoints = cvCreateMat( 1, imgPts->cols, CV_32FC2 );
    objectPoints = cvCreateMat( 1, objPts->cols, CV_32FC3 );
	pointsCounts = cvCreateMat( 1, ptsCts->cols, ptsCts->type);

	cvCopy(intr, intrinsicMatrix);
	cvCopy(dstr, distortionCoeffs);
	cvCopy(objPts, objectPoints);
	cvCopy(imgPts, imagePoints);
	cvCopy(ptsCts, pointsCounts);

	cvReleaseMat(&intr);
	cvReleaseMat(&dstr);
	cvReleaseMat(&objPts);
	cvReleaseMat(&imgPts);
	cvReleaseMat(&ptsCts);
	return 1;


}
void Calibrator::deletePhotos(){
	foreach(QListWidgetItem *item, ui.photos->selectedItems())
	{
		delete (CalibrationPhoto *)item;
	}


}
double Calibrator::calculateReprojectionError( const CvMat* object_points,
        const CvMat* rot_vects, const CvMat* trans_vects,
        const CvMat* camera_matrix, const CvMat* dist_coeffs,
        const CvMat* image_points, const CvMat* point_counts,
        CvMat* per_view_errors )
{
    CvMat* image_points2 = cvCreateMat( image_points->rows,
        image_points->cols, image_points->type );
    int i, image_count = rot_vects->rows, points_so_far = 0;
    double total_err = 0, err;
    
    for( i = 0; i < image_count; i++ )
    {
        CvMat object_points_i, image_points_i, image_points2_i;
        int point_count = point_counts->data.i[i];
        CvMat rot_vect, trans_vect;

        cvGetCols( object_points, &object_points_i,
            points_so_far, points_so_far + point_count );
        cvGetCols( image_points, &image_points_i,
            points_so_far, points_so_far + point_count );
        cvGetCols( image_points2, &image_points2_i,
            points_so_far, points_so_far + point_count );
        points_so_far += point_count;

        cvGetRow( rot_vects, &rot_vect, i );
        cvGetRow( trans_vects, &trans_vect, i );

        cvProjectPoints2( &object_points_i, &rot_vect, &trans_vect,
                          camera_matrix, dist_coeffs, &image_points2_i,
                          0, 0, 0, 0, 0 );
        err = cvNorm( &image_points_i, &image_points2_i, CV_L1 );
        if( per_view_errors )
            per_view_errors->data.db[i] = err/point_count;
        total_err += err;
    }
    
    cvReleaseMat( &image_points2 );
    return total_err/points_so_far;
}