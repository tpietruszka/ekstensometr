#include "collection.h"

#define QMARKERS 1
#define CIRCLEMARKERS 2
Collection::Collection(QWidget *parent)
: QListWidget(parent)
{
	calibrated = 0;
	undistortMapReady=0;
	//intrinsic_matrix = cvCreateMat(3,3,CV_32FC1);
	//distortion_coeffs = cvCreateMat(5,1,CV_32FC1);
	undistortX=NULL;
	undistortY=NULL;

	calibrator = new Calibrator(0);
	connect(calibrator, SIGNAL(off(bool)), this, SLOT(calibrationDone(bool)));


	connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(showPhoto(QListWidgetItem*)));
}

Collection::~Collection()
{
	cvReleaseImage(&undistortX);
	cvReleaseImage(&undistortY);
	if(calibrator)
		delete calibrator;
}
void Collection::loadPhotos()
{
	if(count()==0 && !calibrated)//jesli uzytkownik chce po raz pierwszy dodac zdjecia do nieskalibrowanego systemu - ostrzezenie
	{
		QMessageBox msg;
		msg.setText("Nie skalibrowano ekstensometru!");
		msg.setInformativeText("Uzyskane wyniki b�d� niedok�adne.\n\n Czy mimo to chcesz kontynuowa�?");
		msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msg.setDefaultButton(QMessageBox::Yes);
		int yesNo = msg.exec();
		if(yesNo != QMessageBox::Yes)
			return;
	}
	QStringList newNames = QFileDialog::getOpenFileNames(this, tr("Wybierz zdj�cia"), "C:\\Documents and Settings\\Typ\\Pulpit\\testy\\eksperyment\\dzien3", tr("Pliki obraz�w (*.png *.jpg *.bmp *.tiff *.tif)"));
	if(newNames.isEmpty()){
		return;
	}
	//je�li nie ma jeszcze mapy dystorsji -nale�y j� przygotowa�,
	//dla zdj�cia o rozmiarze pierwszego z wybranych
	if(!undistortMapReady){
		IplImage * tempImage = cvLoadImage(newNames[0].toLatin1());
		imageSize = cvGetSize(tempImage);
		cvReleaseImage(&tempImage);
		prepareUndistort(imageSize);
	}

	//ka�de zdj�cie jest �adowane do pami�ci
	//je�li istnieje i ma poprawne (takie, jak pierwsze) wymiary 
	//to korygowana jest dystorsja i jest dodawane do listy
	foreach(QString newName, newNames){
		Scene * newScene = new Scene(undistortX, undistortY, this);
		if(newScene->loadFromFile(newName)==false || 
			!(newScene->imageSize().height==imageSize.height && newScene->imageSize().width==imageSize.width))
		{
			QMessageBox::warning(0, tr("B��d"), tr("Wybrano niepoprawny plik"));
			delete newScene;
			continue;
		}
		addItem(newScene);



	}

}
void Collection::prepareUndistort(CvSize imageSize){
	if(!calibrated)
		return; //blad, nie powinno dojsc do tej sytuacji
	undistortX = cvCreateImage( imageSize, IPL_DEPTH_32F, 1 );
	undistortY = cvCreateImage( imageSize, IPL_DEPTH_32F, 1 );
	cvInitUndistortMap(	calibrator->intrinsicMatrix, calibrator->distortionCoeffs, undistortX, undistortY);
	undistortMapReady=1;
}

void Collection::calibrate(){

	calibrator->show();
	this->setEnabled(0);
}

void Collection::calibrateFromFile()
{
	if(calibrator->loadFromFile())
	{
		calibrationDone(1);
	}
}
void Collection::calibrationDone(bool success){
	if(success)
	{
		calibrated = true;
	}
	this->setEnabled(1);
}
void Collection::keyPressEvent(QKeyEvent* e){
	if(e->key() == 0x01000007)
	{
		e->accept();
	}else{
		e->ignore();
		return;
	}
	foreach(QListWidgetItem *item, selectedItems())
	{
		delete item;
	}
	for(int i=0; i<count(); i++)
	{
		((Scene*)item(i))->positions.clear();
	}
	pointsPositions.clear();


}
void Collection::showPhoto(QListWidgetItem* photo){
	((Scene*)photo)->preview();

}
QList<QList<CvPoint2D32f>> Collection::getPositions(int markerType, int markerCount, const QList<CvPoint2D32f> referencePoints){

	pointsPositions.clear();

	QList<QList<CvPoint2D32f>> scenesPoints; //indeksowane [scena][nr_punktu]
	Scene* currentScene = NULL;
	//wyznaczanie po�o�e� punkt�w na kolejnych zdj�ciach
	for(int i=0; i< count(); i++){
		currentScene = (Scene*) this->item(i);
		QList<CvPoint2D32f> currentPositions;
		if(markerType == QMARKERS)
			currentScene->qMarkersCorners(currentPositions, markerCount);
		if(markerType == CIRCLEMARKERS)
			currentScene->circleMarkers(currentPositions, markerCount);
		if(currentPositions.count() != markerCount)
		{
			delete currentScene;
		}else{

			scenesPoints.append(currentPositions);

			QString newLabel = currentScene->text(); //wy�wietlanie informacji o post�pie
			newLabel.append(" - OK");
			currentScene->setText(newLabel);
			currentScene->listWidget()->scrollToItem(currentScene);
			repaint();
		}


	}

	int pointsCount = scenesPoints.at(0).count();
	//kolejno�� punkt�w na pierwszym zdj�ciu - dowolna, albo wg dostarczonej listy
	for(int i=0; i< pointsCount; i++)
	{
		QList<CvPoint2D32f> newPoint;//lista po�o�e� danego punktu
		if(referencePoints.count() == 0)
		{
			newPoint.append(scenesPoints[0][i]);
		}else{
			newPoint.append(stereoMatchingPoint(scenesPoints[0], referencePoints[i]));
		}
		pointsPositions.append(newPoint);
	}

	//odnalezienie odpowiadaj�cych sobie punkt�w na kolejnych zdj�ciach 
	for(int i=1; i<scenesPoints.count(); i++){ //dla ka�dego zdj�cia (poza nr 0)
		int j=0;
		while(scenesPoints[i].count() > 0){  //dla ka�dego punktu na tym zdj�ciu (odnajdywanie odpowiadaj�cych sobie p. usuwa je ze scenesPoints)
			pointsPositions[j].append(nearestPoint(scenesPoints[i], pointsPositions[j][i-1]));
			j++;
		}
	}
	for(int i=0; i<count(); i++)
	{
		QList<CvPoint2D32f> scenePointsList;
		for(int j=0; j< pointsCount; j++){
			scenePointsList.append(pointsPositions[j][i]);
		}
		((Scene*)item(i))->positions = scenePointsList;
	}
	return pointsPositions;
}

double Collection::pointsDistance(CvPoint2D32f a, CvPoint2D32f b)
{
	return sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}
CvPoint2D32f Collection::nearestPoint(QList<CvPoint2D32f>&list, CvPoint2D32f base)
{
	double distance=1/DBL_EPSILON;
	double temp;
	double index=0;
	for(int i=0; i<list.size(); i++)
	{
		temp = pointsDistance(list[i], base);
		if( temp < distance )
		{
			distance = temp;
			index = i;
		}
	}
	return list.takeAt(index);
}

CvPoint2D32f Collection::stereoMatchingPoint(QList<CvPoint2D32f>& list, CvPoint2D32f base){
	double distance=1/DBL_EPSILON;
	double temp;
	double index=0;
	for(int i=0; i<list.size(); i++)
	{
		temp = this->stereoPointsDistance(list[i], base);
		if( temp < distance )
		{
			distance = temp;
			index = i;
		}
	}
	return list.takeAt(index);
}
double Collection::stereoPointsDistance(const CvPoint2D32f a, const CvPoint2D32f b)
{
	return sqrt((a.x-b.x)*(a.x-b.x)*0.5 + (a.y-b.y)*(a.y-b.y));
}
