#pragma once

#include <QtGui/QMainWindow>
#include <QImage>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QTimer>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QShortcut>
#include <QKeySequence>

#include "ui_calibrator.h"
#include "opencv/highgui.h"
#include "opencv/cv.h"
#include "opencv/cxcore.h"

#include "calibration_photo.h"

//! Klasa dostarczaj�ca informacji o Intrinsic parameters i dystorsji

//! Oblicza parametry pojedynczej kamery na podstawie zdj��, lub wczytuje z pliku
//! Wczytywane informacje zawieraj� po�o�enia naro�nik�w, kt�re s� p�niej wykorzystywane do stereokalibracji
class Calibrator : public QWidget
{
	Q_OBJECT
public:
	Calibrator(QWidget* parent=0);
	~Calibrator(void);
	
	//! Wczytuje dane z pliku
	bool loadFromFile();

	CvSize boardSize;
	CvSize imageSize;
	double squareSize; //d�ugo�� boku kwadratu szachownicy

	CvMat* intrinsicMatrix;
	CvMat* distortionCoeffs;
	CvMat* imagePoints;
	CvMat* objectPoints;
	CvMat* pointsCounts;
	bool calibrated; //czy jest skalibrowane?
	double reprojectionError;

	//! Liczy �redni b��d reprojekcji
	double calculateReprojectionError( const CvMat* object_points,
        const CvMat* rot_vects, const CvMat* trans_vects,
        const CvMat* camera_matrix, const CvMat* dist_coeffs,
        const CvMat* image_points, const CvMat* point_counts,
        CvMat* per_view_errors );

public slots:
	//! usuni�cie wszystkich zdj��, reset parametr�w
	void reset();
	//! zapis parametr�w do pliku
	void saveToFile();
	//! wczytanie zdj�� (w�a�ciwie: nazw, utworzenie obiekt�w reprezentuj�cych je)
	void loadPhotos();
	//! obliczenia kalibracyjne, w razie sukcesu zmienia flag� "calibrated" na true
	void calibrate();
	//! usuniecie zdjec z listy
	void deletePhotos();
signals:
	void off(bool success); //wysyla rozmiar zdjecia jesli skalibrowano, 0 i 0 w przeciwnym wypadku



private:
	
	void paintEvent( QPaintEvent * event );

	//! emituje sygnal o skonczeniu kalibracji (i jej wyniku)
	void closeEvent( QCloseEvent *event );

	QShortcut * photoDeleteShortcut;
	Ui::Calibrator ui;
	
	int cornersOnBoard;

	


};
