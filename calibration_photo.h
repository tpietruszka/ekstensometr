#ifndef CALIBRATIONPHOTO_H
#define CALIBRATIONPHOTO_H

#include <QListWidgetItem>
#include <QString>
#include <QFileInfo>

#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv/cxcore.h"
//! Klasa odpowiedzialna za wczytywanie zdj�� kalibracyjnych i znajdowanie wzorca kalibracji

class CalibrationPhoto : public QListWidgetItem
{


public:
	//! Zapisuje nazw� pliku i oczekiwan� wielko�� szachownicy, nie wczytuje pliku
	CalibrationPhoto(QString fileName, QListWidget *parent);
	~CalibrationPhoto();

	//! Analizuje zdj�cie, zapisuje wynik w "corners", zwraca (bool) success
	bool analyse(CvSize p_boardSize);


	QString fileName;
	CvSize boardSize;
	CvSize imageSize;
	CvPoint2D32f * corners;

private:
	
	
};

#endif // CALIBRATIONPHOTO_H
