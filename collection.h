#ifndef COLLECTION_H
#define COLLECTION_H

#include <QListWidget>
#include <QTableWidgetItem>
#include <QMessageBox>

#include "opencv/highgui.h"
#include "opencv/cv.h"

#include "calibrator.h"
#include "scene.h"

//! Obiekt reprezentuj�cy seri� zdj�� i odpowiadaj�ce im dane kalibracyjne. Posiada w�asn� instancj� kalibratora
class Collection : public QListWidget
{
	Q_OBJECT

public:
	Collection(QWidget *parent);
	~Collection();
	Calibrator * calibrator;
	//! flaga - czy ju� skalibrowane?
		bool calibrated;

	public slots:

		//! otwiera kalibrator
		void calibrate();
		//! wysy�a sygna� do wczytania danych 
		void calibrateFromFile();
		//! odbiera sygna� z kalibratora, zmienia flag� "calibrated"
		void calibrationDone(bool success);
		//! wczytuje nazwy zdj��
		void loadPhotos();
		//! pokazuje podgl�d zdj�cia
		void showPhoto(QListWidgetItem* photo);
		//! zwraca pozycje punkt�w w formie lista[punkt][numer_zdjecia]. Odpowiedzialna za �ledzenie punkt�w i stereo-matching (z list� referencePoints)
		//! dla pierwszej kamery jako referencePoints nale�y przekaza� pust� list�
		QList<QList<CvPoint2D32f>> getPositions(int markerType, int markerCount, const QList<CvPoint2D32f> referencePoints);


		
private:
	

protected:
	void keyPressEvent(QKeyEvent* event);

	
	void prepareUndistort(CvSize Size);

	
	//CvMat* intrinsic_matrix;
	//CvMat* distortion_coeffs;	
	IplImage * undistortX, * undistortY;

	QList<QList<CvPoint2D32f>> pointsPositions; //zapisuje wyniki getPositions  //indeksowane [nr_punktu][nr_sceny], 

	bool undistortMapReady;
	CvSize imageSize;//rozmiar analizowanych zdjec


	//! Zwraca punkt, kt�ry odpowiada danemu (lewemu) na prawym zdj�ciu stereo
	CvPoint2D32f stereoMatchingPoint(QList<CvPoint2D32f>& list, CvPoint2D32f base);
	//! Zwraca miar� odleg�o�ci u�ywan� w dopasowaniu punkt�w pomi�dzy zdj�ciami stereowizyjnymi
	double stereoPointsDistance(const CvPoint2D32f a, const CvPoint2D32f b);


	//! zwraca odleg�o�� mi�dzy 2 punktami
	static inline double pointsDistance(CvPoint2D32f a, CvPoint2D32f b);
	//! Z danej listy punkt�w wybiera punkt najbli�szy base; UWAGA - USUWA TEN PUNKT Z LISTY
	CvPoint2D32f nearestPoint(QList<CvPoint2D32f>& list, CvPoint2D32f base);


	

};

#endif // COLLECTION_H
