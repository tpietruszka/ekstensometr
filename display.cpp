#include "display.h"

Display::Display(QList<QList<CvPoint3D64f>> data, QWidget *parent)
: QWidget(parent)
{
	ui.setupUi(this);
	setWindowTitle("Wyniki pomiar�w");

	output=NULL;
	pointsPositions = data;
	pointsCount = pointsPositions.count();
	scenesCount = pointsPositions.at(0).count();

	ui.point1->setMaximum(pointsCount);
	ui.point2->setMaximum(pointsCount);
	ui.resultsTable->setRowCount(scenesCount);
	ui.resultsTable->setColumnCount(1);
	QStringList tableLabels;
	tableLabels <<"Warto��";
	ui.resultsTable->setHorizontalHeaderLabels(tableLabels);


	connect(ui.displayButton, SIGNAL(clicked()), this, SLOT(doDisplay()));
	connect(ui.copyButton, SIGNAL(clicked()), this, SLOT(copyResults()));
	connect(ui.exportPointsButton, SIGNAL(clicked()), this, SLOT(exportPoints()));
}

Display::~Display()
{
	if(output)
		delete output;

}
void Display::doDisplay()
{

	int point1 = ui.point1->value();
	int point2 = ui.point2->value();
	if (point1 == point2 || point1 > pointsCount ||point2 > pointsCount)
	{
		QMessageBox::warning(NULL, "Blad", "Wybrano niepoprawne punkty");
		return;
	}

	if(output)
		delete output;
	output = new Plot(ui.graphArea);

	pointsDistances.clear();
	pointsDistances.resize(scenesCount);


	for(int i=0; i<scenesCount; i++)
	{
		pointsDistances[i]=pointsDistance(pointsPositions[point1-1][i], pointsPositions[point2-1][i]);
	}
	QVector<double> results;

	if(ui.typeDistance->isChecked())
	{
		results = pointsDistances;
		output->setupPlot("Odleg�o�ci", "Numer sceny", "Odleg�o�� [mm]");
	}
	if(ui.typeDisplacement->isChecked())
	{
		results = displacement();
		output->setupPlot("Przemieszczenia (marker 1)", "Numer sceny", "Odleg�o�� [mm]");
	}
	if(ui.typeEngineeringStrain->isChecked())
	{
		results = engineeringStrain();
		output->setupPlot("Odkszta�cenie in�ynierskie", "Numer sceny", "Odkszta�cenie");
	}
	if(ui.typeNaturalStrain->isChecked())
	{
		results = naturalStrain();
		output->setupPlot("Odkszta�cenie naturalne", "Numer sceny", "Odkszta�cenie");
	}
	if(ui.typeLagrangeStrain->isChecked())
	{
		results = lagrangeStrain();
		output->setupPlot("Odkszta�cenie Lagrange'a", "Numer sceny", "Odkszta�cenie");
	}
	if(ui.typeStretch->isChecked())
	{
		results = stretch();
		output->setupPlot("Rozci�gni�cie", "Numer sceny", "Rozci�gni�cie");
	}

	ui.resultsTable->clear();
	for(int i=0; i< scenesCount; i++)
	{
		QTableWidgetItem * newResult = new QTableWidgetItem(loc.toString((double)results[i], 'g', 7));
		ui.resultsTable->setItem(i, 0, newResult);

	}
	output->show();
	output->displayResults(results);
	output->repaint();




}

double Display::pointsDistance(CvPoint3D64f a, CvPoint3D64f b){
	return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z));

}
double Display::pointsDistance(CvPoint2D64f a, CvPoint2D64f b){
	return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));

}
QVector<double> Display::displacement()
{
	QVector<double> results(scenesCount);
	int point1 = ui.point1->value();
	results[0]=0;
	for(int i=1; i<scenesCount; i++)
	{
		results[i] = pointsDistance(pointsPositions[point1-1][i-1], pointsPositions[point1-1][i]);
	}
	return results;
}
QVector<double> Display::engineeringStrain()
{
	QVector<double> results(scenesCount);
	double ref = pointsDistances[0];
	for(int i=0; i<scenesCount; i++)
	{
		results[i] = (pointsDistances[i]-ref)/ref;
	}
	return results;

}

QVector<double> Display::naturalStrain()
{
	QVector<double> results(scenesCount);
	double ref = pointsDistances[0];
	for(int i=0; i<scenesCount; i++)
	{
		results[i] = std::log(pointsDistances[i]/ref);
	}

	return results;

}
QVector<double> Display::lagrangeStrain()
{
	QVector<double> results(scenesCount);
	double ref = pointsDistances[0];
	for(int i=0; i<scenesCount; i++)
	{
		results[i] = 0.5*((pointsDistances[i]/ref)*(pointsDistances[i])/ref - 1);
	}

	return results;

}
QVector<double> Display::stretch()
{
	QVector<double> results(scenesCount);
	double ref = pointsDistances[0];
	for(int i=0; i<scenesCount; i++)
	{
		results[i] = pointsDistances[i]/ref;
	}

	return results;

}
void Display::copyResults()
{
	QList<QTableWidgetItem *> selected(ui.resultsTable->selectedItems());
	QTableWidgetItem *item;

	QString textOutput;
	foreach(item, selected)
	{
		textOutput.append(item->text());
		textOutput.append("\r\n");

	}
	QApplication::clipboard()->setText(textOutput);
}
void Display::exportPoints()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Zapisz plik", "", ".csv");
	if(fileName.isEmpty())
		return;
	 QFile file(fileName);
     if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
         return;

     QTextStream out(&file);
	 out << "// pointsCount, scenesCount, points[pointNr][sceneNr]: .x .y .z " << endl;
	 out << pointsCount << ", " << scenesCount << endl;
	 for(int i=0; i< pointsCount; i++)
	 {
		 for(int j=0; j< scenesCount; j++)
			 out << pointsPositions[i][j].x << ", " << pointsPositions[i][j].y << ", " << pointsPositions[i][j].z << endl;
		 out << endl;

	 }


}