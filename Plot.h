#pragma once
#include <QWidget>
#include "qwt_plot.h"
#include "qwt_legend.h"
#include "qwt_plot_curve.h"
#include "qwt_series_data.h"
//! klasa odpowiedzialna za wyświetlanie wykresów w QWT_Plot
class Plot :
	public QwtPlot
{
Q_OBJECT
public:
	Plot(QWidget * parent=0);
	~Plot(void);
	void setupPlot(QString title, QString xLabel, QString yLabel);
	void displayResults(QVector<double> &data, int whichAxis=1);
private:
	QwtPlotCurve * plotCurve;

};
