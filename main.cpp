﻿#include "extensometer.h"
#include <QtGui/QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("cp1250"));
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("cp1250"));
	QTextCodec::setCodecForTr(QTextCodec::codecForName("cp1250"));
	Extensometer w;
	w.show();
	return a.exec();
}
