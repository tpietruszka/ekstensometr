#include "calibration_photo.h"

CalibrationPhoto::CalibrationPhoto(QString fileName, QListWidget *parent)
	: QListWidgetItem(parent)
{
	this->fileName = fileName;
	this->corners = NULL;
	QFileInfo fileInfo(fileName);
	this->setText(fileInfo.fileName());
	imageSize = cvSize(0, 0);

}

CalibrationPhoto::~CalibrationPhoto()
{
	if(corners)
		delete corners;

}

bool CalibrationPhoto::analyse(CvSize p_boardSize){
	IplImage * image;
	IplImage * grayImage;

	this->boardSize = p_boardSize;

	image = cvLoadImage(fileName.toLatin1(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	if(!image)
		return false;

	//wymagane przez kalibrator
	imageSize = cvGetSize(image);
	corners = new CvPoint2D32f[boardSize.height*boardSize.width];
	int cornerCount, found;

	found = cvFindChessboardCorners(image, boardSize, corners, &cornerCount, CV_CALIB_CB_ADAPTIVE_THRESH |CV_CALIB_CB_NORMALIZE_IMAGE);
	if(!found || (cornerCount != (boardSize.height*boardSize.width)))
		return false;

	grayImage = cvCreateImage(cvGetSize(image),8,1);//do analizy subpixelowej

	if(image->nChannels ==1)
		cvCopy(image, grayImage);
	else
		cvCvtColor(image, grayImage, CV_BGR2GRAY);
	cvFindCornerSubPix( grayImage, corners, cornerCount, cvSize(11,11),
            cvSize(-1,-1), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.01 ));


	cvReleaseImage(&image);
	cvReleaseImage(&grayImage);
	QString newLabel = this->text();
	newLabel.append(" - OK -");
	this->setText(newLabel);
	this->listWidget()->repaint();
	return true;
}
