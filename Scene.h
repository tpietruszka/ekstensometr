#pragma once
#include<cmath>
#include<QtGui>
#include<QPixmap>
#include<QListWidgetItem>
#include <QFileInfo>

#include<cstring>
#include"opencv/highgui.h"
#include"opencv/cv.h"
class Scene : public QListWidgetItem
{
public:
	Scene(IplImage * undistortX, IplImage * undistortY, QListWidget * parent);
	~Scene(void);
	//! konwersja obrazu z IplImage do QImage
	static void ipl2q(IplImage *iplImg, QImage * qimg, int w,int h, int channels);
	//! Wykrywa zadan� ilo�� okr�g�w na obrazie. Warto�ci� zwracan� s� ich promienie. Dzia�anie przybli�one
	QList<double> circlesApprox(QList<CvPoint2D32f>& positions, int markersDeclared);

	//! Ograniczona dok�adno��, do zmiany lub usuni�cia, nieu�ywana
	void qMarkersLineFitting(QList<CvPoint2D32f>& positions, int markersDeclared);

	//! Podstawowa metoda detekcji qmarker�w
	void qMarkersCorners(QList<CvPoint2D32f>& positions, int markersDeclared);

	//! detekcja marker�w okr�g�ych - subpikselowa
	void circleMarkers(QList<CvPoint2D32f>& positions, int markersDeclared);
	//! wczytywanie zdj�cia
	bool loadFromFile(QString fileName);
	//! podgl�d w nowym oknie
	void preview();
	CvSize imageSize(){return cvGetSize(image);};
	
	QList<CvPoint2D32f> positions; //wynikowe pozycje punkt�w, ustawione w kolejno�ci wg pierwszej sceny (zewn�trznie przez kolekcj�)
private:
	double pointsDistance(CvPoint2D32f a, CvPoint2D32f b);
	double linePointDistance(CvPoint2D32f line1, CvPoint2D32f line2, CvPoint2D32f point);
	double linesAngle(CvPoint2D32f line11, CvPoint2D32f line12, CvPoint2D32f line21, CvPoint2D32f line22); 
	CvPoint2D32f crossingPoint(CvPoint2D32f line11, CvPoint2D32f line12, CvPoint2D32f line21, CvPoint2D32f line22);
	IplImage * image;
	IplImage * grayImage;
	IplImage * undistortX;
	IplImage * undistortY; //wynik kalibracji, uzywany w remap()
	

};
