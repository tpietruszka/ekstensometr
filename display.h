#ifndef DISPLAY_H
#define DISPLAY_H
#include <cmath>

#include <QWidget>
#include <QList>
#include <QVector>
#include <QMessageBox>
#include <QClipboard>
#include <QLocale>
#include <QFileDialog>

#include "opencv/cv.h"
#include "opencv/cxcore.h"

#include "ui_display.h"
#include "plot.h"
//! Klasa na podstawie posiadanych po�o�e� punkt�w w 3D wy�wietla wyniki: odleg�o�ci, przemieszczenia, odkszta�cenia. Umo�liwia te� eksport po�o�e� do pliku
class Display : public QWidget
{
	Q_OBJECT

public:
	Display(QList<QList<CvPoint3D64f>> data, QWidget *parent = 0);
	~Display();

	//! Zwraca odleg�o�� mi�dzy dwoma punktami
	static double pointsDistance(CvPoint3D64f a, CvPoint3D64f b);
	static double pointsDistance(CvPoint2D64f a, CvPoint2D64f b);

	private slots:
	//! Sprawdza dane wej�ciowe i wywo�uje odpowiedni� funkcj� wy�wietlaj�c� wyniki
	void doDisplay();
	//! Kopiuje zawarto�� tabeli z wynikami do schowka
	void copyResults();
	//! Zapisuje ilo�� scen, punkt�w na scenie, a nast�pnie wsp�rz�dne punkt�w do pliku
	void exportPoints();

	


private:
	//! na podstawie this->pointsDistances i this-> pointsPositions funkcje licz� i zwracaj� odpowiednie warto�ci
	QVector<double> engineeringStrain();
	QVector<double> naturalStrain();
	QVector<double> lagrangeStrain();
	QVector<double> stretch();
	QVector<double> displacement();

	//! Obiekt wywo�ywany �eby w�a�ciwie formatowa� wy�wietlane liczby
	QLocale loc;


	Ui::Display ui;
	//! wykres
	Plot * output;
	//! dane pobierane w konstruktorze 
	QList<QList<CvPoint3D64f>> pointsPositions;
	//! tymczasowe - odleg�o�ci mi�dzy wybranymi punktami
	QVector<double> pointsDistances;
	int pointsCount;
	int scenesCount;
};

#endif // DISPLAY_H
