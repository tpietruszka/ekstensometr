﻿#include "plot.h"

Plot::Plot(QWidget * parent) : QwtPlot(parent)
{
	plotCurve = NULL;
	this->resize(800, 600);
}

Plot::~Plot(void)
{
	if(plotCurve)
		delete plotCurve;
}
void Plot::setupPlot(QString title, QString xLabel, QString yLabel) {

    this->setTitle(title);
    this->setCanvasBackground(QColor(Qt::white));

    this->setAutoReplot(false);
    //this->setMargin(10);

    

    // axis
    this->setAxisTitle(QwtPlot::xBottom, xLabel);
    this->setAxisTitle(QwtPlot::yLeft, yLabel);
	

}
void Plot::displayResults(QVector<double> &data, int whichAxis)
{
	if(plotCurve)
		delete plotCurve;
	plotCurve = new QwtPlotCurve();
	int howMany = data.size();
	this->setAxisScale(QwtPlot::xBottom, 0, howMany-1, 1);
	this->setAxisAutoScale(QwtPlot::yLeft, true);

	QVector<double> indexes(howMany);
	for(int i=0; i<howMany;i++){
		indexes[i]=i;
	}
	plotCurve->setSamples(indexes.data(), data.data(), howMany);
	plotCurve->attach(this);
	replot();
}