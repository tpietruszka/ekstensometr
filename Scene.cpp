﻿#include "scene.h"
struct linePosition{
	int index;
	double distance;
};
using namespace cv;
Scene::Scene(IplImage * undistortX, IplImage * undistortY, QListWidget * parent) : QListWidgetItem(parent)
{
	this->undistortX = undistortX;
	this->undistortY = undistortY;

	image = NULL;
	grayImage = NULL;
	//this->resize(w,h);

}

Scene::~Scene(void)
{
	cvReleaseImage(&image);
	cvReleaseImage(&grayImage);
}
//! Ładuje zdjęcie z wybranego pliku, koryguje dystorsję
/**
Zwraca true w przypadku sukcesu
*/

bool Scene::loadFromFile(QString fileName){
	if(fileName.isEmpty())
		fileName = QFileDialog::getOpenFileName(NULL, "Open image", "C:\\Documents and Settings\\Typ\\Pulpit\\testy\\eksperyment\\dzien3", ("Image Files (*.png *.jpg *.bmp *.gif *.tiff)"));
	if(fileName.isEmpty())
		return 0;

	image = cvLoadImage(fileName.toLatin1(), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	if(undistortX  && undistortY)
	{
		//IplImage *t = cvCloneImage(image);
		//cvRemap( t, image, undistortX, undistortY ); // Undistort image
	}
	
	QFileInfo fileInfo(fileName);
	this->setText(fileInfo.fileName());
	return 1;

}
void Scene::qMarkersCorners(QList<CvPoint2D32f>& positions, int markersDeclared)
{
	if(!image){
		QMessageBox::warning(NULL, "Blad", "Obraz do analizy niedostepny");
		return;
	}

	QList<CvPoint2D32f> circles;
	QList<double> circlesRadius;
	circlesRadius = circlesApprox(circles, markersDeclared); //wykrywanie okregow

	grayImage = cvCreateImage(cvGetSize(image), 8, 1);
	IplImage * processedImage = cvCreateImage(cvGetSize(image), 8, 1);
	

	if(image->nChannels ==1)
		cvCopy(image, grayImage);
	else
		cvCvtColor(image, grayImage, CV_BGR2GRAY);
	cvSmooth(grayImage, processedImage, CV_GAUSSIAN, 5,5 );
	
	positions.clear();
	int x, y, w,h;
	CvPoint2D32f* potentialPositions = new CvPoint2D32f[5];
	for(int i=0; i<circles.count(); i++)
	{
		CvPoint2D32f currentPosition;
		x = cvRound(circles[i].x - 1.5*circlesRadius[i]); //obszar poszukiwania narożnika
		x = (x < 0 ? 0 : x);
		y = cvRound(circles[i].y - 1.5*circlesRadius[i]);
		y = (y < 0 ? 0 : y);

		w=h=3*cvRound(circlesRadius[i]);
		if(x+w >= image->width)
			w = image->width - x-1;
		if(y+h >= image->height)
			h= image->height - y-1;
		cvSetImageROI(processedImage, cvRect(x, y, w, h));
		int featuresCount = 5;//wartość maksymalna
		for(int p=0; p < 5; p++) potentialPositions[p] =cvPoint2D32f(-1, -1);
		//poszukiwanie narożników na wybranym obszarze (zgrubne)

		cvGoodFeaturesToTrack(processedImage, NULL, NULL, potentialPositions, &featuresCount, 0.1, 4);
		
		double minDist = 1/DBL_EPSILON;
		for(int p=0; p < 5; p++)		//wyszukiwanie punktu najbliższego środka
		{
			CvPoint2D32f potentialAbs; //polozenie w globalnych wspolrzednych
			potentialAbs.x = potentialPositions[p].x + x;
			potentialAbs.y = potentialPositions[p].y + y;
			if(pointsDistance(potentialAbs, circles[i]) < minDist)
			{
				currentPosition = potentialAbs;
				minDist = pointsDistance(potentialAbs, circles[i]);
			}
		}

		//wyznaczanie subpikselowe
		cvFindCornerSubPix(grayImage, &currentPosition, 1, cvSize(4, 4), cvSize(-1, -1), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.01 ));
		positions.push_back(currentPosition);
	}
	delete [] potentialPositions;
}



void Scene::qMarkersLineFitting(QList<CvPoint2D32f>& positions, int markersDeclared){

	if(!image){
		QMessageBox::warning(NULL, "Blad", "Obraz do analizy niedostepny");
		return;
	}
	QList<CvPoint2D32f> circles;
	circlesApprox(circles, markersDeclared); //wykrywanie okregow


	CvMemStorage * storage = cvCreateMemStorage();
	grayImage = cvCreateImage(cvGetSize(image), 8, 1);
	if(image->nChannels ==1)
		cvCopy(image, grayImage);
	else
		cvCvtColor(image, grayImage, CV_BGR2GRAY);

	IplImage * edges = cvCreateImage(cvGetSize(image), 8,1);

	cvSmooth(grayImage, grayImage, CV_GAUSSIAN, 11,11 );
	cvCanny(grayImage, edges, (double)60, (double)170, 3);	
	//cvMorphologyEx(edges, edges, NULL, NULL, CV_MOP_OPEN);

	CvSeq * lines=0;


	IplImage * smallPrev = cvCreateImage(cvSize(800, 600), 8, 1);
	/*PREVIEW
	cvResize(edges, smallPrev);
	cvShowImage("canny", smallPrev);*/
	int maxDim = (edges->width > edges->height ? edges->width : edges->height);
	lines = cvHoughLines2(edges, storage, CV_HOUGH_PROBABILISTIC, 1, CV_PI/720, maxDim/40, maxDim/80, 10);
	//wyszukane linie
	CvPoint * line;
	CvPoint2D32f line1, line2, circle;
	double tempDist;

	cvZero(edges);
	for(int c=0; c<circles.count(); c++)
	{
		QList<linePosition> lineIndex; // przechowuje indeksy linii i odległości od środka okręgu
		for(int l=0; l<lines->total; l++)
		{
			line = (CvPoint*) cvGetSeqElem(lines, l);
			line1 = cvPoint2D32f(line[0].x, line[0].y);
			line2 = cvPoint2D32f(line[1].x, line[1].y);
			circle =  cvPoint2D32f(circles.at(c).x, circles.at(c).y);//srodek!
			linePosition newLine;
			newLine.index = l;
			newLine.distance = linePointDistance(line1, line2, circle);	
			int i=0;
			while(i<lineIndex.count() && newLine.distance > lineIndex.at(i).distance)
				i++; //lista linii posortowana wg odleglosci od punktu - wyszukiwanie miejsca do wstawienia
			lineIndex.insert(i, newLine);

			/*PREVIEW
			cvLine(edges, line[0], line[1], cvScalar(255, 255, 255));
			cvCircle(  edges,  circles->at(c),  100,  cvScalar(255, 255, 255),  1,  8,  0 );
			cvResize(edges, smallPrev);
			cvShowImage("lines", smallPrev);*/
			//cvWaitKey(1000);
		}
		CvPoint2D32f line11, line12, line21, line22;

		line = (CvPoint*) cvGetSeqElem(lines, lineIndex.at(0).index);
		line11 = cvPoint2D32f(line[0].x, line[0].y);
		line12 = cvPoint2D32f(line[1].x, line[1].y);

		double angle=0;
		int i=1;
		while(angle<75*M_PI/180 && i <lines->total)
		{

			line = (CvPoint*) cvGetSeqElem(lines, lineIndex.at(i).index);
			line21 = cvPoint2D32f(line[0].x, line[0].y);
			line22 = cvPoint2D32f(line[1].x, line[1].y);
			angle = linesAngle(line11, line12, line21, line22);
			i++;
		}
		CvPoint2D32f currentPosition = crossingPoint(line11, line12, line21, line22);
		cvFindCornerSubPix(grayImage, &currentPosition, 1, cvSize(4, 4), cvSize(-1, -1), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.01 ));
		positions.push_back(currentPosition);
	}



	/*for(int i=0; i<lines->total; i++)
	{
	line = (CvPoint*) cvGetSeqElem(lines, i);
	cvLine(edges, line[0], line[1], cvScalar(255, 255, 255));
	}
	cvShowImage("lines", edges);*/


	//CvSeq* contours;
	//int count; //ile znaleziono konturow
	//count= cvFindContours(edges, storage, &contours, sizeof(CvContour), CV_RETR_LIST);
	//cvZero(edges);
	//for( CvSeq* c=contours; c!=NULL; c=c->h_next ) {
	//	
	//	cvDrawContours(edges, c, cvScalar(255, 0, 0), cvScalar(0, 0, 255), 0,  1, 8 );
	//		cvShowImage("sobel", edges);
	//	cvWaitKey(1000);
	//}
	//CvSize imageSize = cvGetSize(image);
	cvReleaseImage(&edges);
	return;
}
//odleglosc miedzy punktami
double Scene::pointsDistance(CvPoint2D32f a, CvPoint2D32f b){
	return sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}
//zwraca kat (wart. absolutna) miedzy liniami, danymi jako 4 punkty
double Scene::linesAngle(CvPoint2D32f line11, CvPoint2D32f line12, CvPoint2D32f line21, CvPoint2D32f line22)
{
	double alfa1 = atan((line12.y-line11.y)/(line12.x-line11.x));
	double alfa2 = atan((line22.y-line21.y)/(line22.x-line21.x));
	double angle = fabs(alfa2-alfa1);
	if (angle> M_PI/2)
		angle = M_PI - angle;
	return angle;

}
//odleglosc miedzy linia (dana jako 2 punkty) i punktem
double Scene::linePointDistance(CvPoint2D32f line1, CvPoint2D32f line2, CvPoint2D32f point)
{
	double a, b;//wspolczynniki opisujace prosta
	double A, B;//wspolczynniki opisujace prosta prostopadla, przechodzaca przez punkt
	double x, y; //wspolrzedne punktu przeciecia

	if((line2.x-line1.x)==0)
		a=(line2.y-line1.y)/(line2.x-line1.x + DBL_EPSILON);
	else
		a=(line2.y-line1.y)/(line2.x-line1.x);
	b=line1.y-(line1.x*a);
	if(a==0)
		A=-1/(a+DBL_EPSILON);
	else
		A=-1/a;
	B=point.y-(point.x*A);

	x=(B-b)/(a-A);
	double y2=A*x+B; // do debugowania

	if(a==0)
		y=a*x+b; //nieskonczone A powoduje bledy
	else
		y=A*x+B;

	double distance;
	if(!((x>line1.x+DBL_EPSILON && x > line2.x+DBL_EPSILON)  || (x+DBL_EPSILON < line1.x && x+DBL_EPSILON< line2.x))) 
	{
		distance = pointsDistance(cvPoint2D32f(x, y), point);
	}else{
		int temp = pointsDistance(line1, point);
		distance = pointsDistance(line2, point);
		if(temp<distance) distance = temp;
	}
	return distance;


}
CvPoint2D32f Scene::crossingPoint(CvPoint2D32f line11, CvPoint2D32f line12, CvPoint2D32f line21, CvPoint2D32f line22)
{
	double a, b;//wspolczynniki opisujace prosta 1
	double A, B;//wspolczynniki opisujace prosta 2
	double x, y; //wspolrzedne punktu przeciecia

	//Współczynniki linii 1
	if((line12.x-line11.x)==0)
		a=(line12.y-line11.y)/(line12.x-line11.x + DBL_EPSILON);
	else
		a=(line12.y-line11.y)/(line12.x-line11.x);
	b=line11.y-(line11.x*a);

	//Współczynniki linii 2
	if((line22.x-line21.x)==0)
		A=(line22.y-line21.y)/(line22.x-line21.x + DBL_EPSILON);
	else
		A=(line22.y-line21.y)/(line22.x-line21.x);
	B=line21.y-(line21.x*A);


	x=(B-b)/(a-A);
	double y2=A*x+B; // do debugowania

	if(a==0)
		y=a*x+b; //nieskonczone A powoduje bledy
	else
		y=A*x+B;
	return cvPoint2D32f(x, y);

}
QList<double>  Scene::circlesApprox(QList<CvPoint2D32f>& positions, int markersDeclared){


	QList<double> radii;
	int declaredCircles = markersDeclared;
	if(!image){
		QMessageBox::warning(NULL, "Blad", "Obraz do analizy niedostepny");
		return radii;
	}
	CvSeq * circles;
	CvMemStorage * storage = cvCreateMemStorage();
	grayImage = cvCreateImage(cvGetSize(image), 8, 1);

	// normalizacja histogramu, filtr dolnoprzepustowy
	if(image->nChannels ==1)
		cvCopy(image, grayImage);
	else
		cvCvtColor(image, grayImage, CV_BGR2GRAY);
	cvNormalize(grayImage, grayImage, 0, 255, CV_MINMAX);
	cvSmooth(grayImage, grayImage, CV_GAUSSIAN, 9,9 );


	//ustalenie parametrów do HoughTransform w zależności od rozmiarów obrazu (szacunkowe wielkości markerów)
	CvSize imageSize = cvGetSize(image);
	int maxDimension = (imageSize.width > imageSize.height ? imageSize.width : imageSize.height);
	int minDist = maxDimension/15;
	int minSize = 5;
	int maxSize = maxDimension/10;
	int minVotes = 30;
	circles = cvHoughCircles(grayImage, storage, CV_HOUGH_GRADIENT, 1, minDist, 200, minVotes, minSize, maxSize);
	if(circles->total < declaredCircles)
	{
		while(circles->total < declaredCircles)
		{
			circles = cvHoughCircles(grayImage, storage, CV_HOUGH_GRADIENT, 1, minDist, 200, minVotes--, minSize, maxSize);
		}
	}else{
		while(circles->total > declaredCircles)
		{
			circles = cvHoughCircles(grayImage, storage, CV_HOUGH_GRADIENT, 1, minDist, 200, minVotes++, minSize, maxSize);
		}
	}
	if(circles->total != declaredCircles)
	{
		circles = NULL;
		QMessageBox::warning(NULL, "Blad", "Wykryto zla ilosc znacznikow");
		return radii;
	}
	if(positions.count() > 0)
		positions.clear();	

	for(  int i  =  0; i < circles-> total; i++ )
	{
		float* p  =  ( float*)cvGetSeqElem(  circles,  i );
		positions.push_back(cvPoint2D32f(  p[0],  p[1]));
		radii.push_back(p[2]);
		//cvCircle(  grayImage,  cvPoint( cvRound( p[0]), cvRound( p[1])),  2,  CV_RGB( 255, 0, 0),  -1,  8,  0 );
		//cvCircle(  grayImage,  cvPoint( cvRound( p[0]), cvRound( p[1])),  cvRound( p[2]),  CV_RGB( 255, 0, 0),  1,  8,  0 );
	} 

	/*cvNamedWindow("circles");
	IplImage * smallPrev = cvCreateImage(cvSize(800, 600), 8, 1);
	cvResize(grayImage, smallPrev);
	cvShowImage("circles", smallPrev);
	cvWaitKey();*/


 
	return radii;

}

void Scene::ipl2q(IplImage *iplImg, QImage * qimg,  int w, int h, int channels) //konwersja z iplImage do Qimage
{

	char *data = iplImg->imageData;

	for (int y = 0; y < h; y++, data += iplImg->widthStep)
	{
		for (int x = 0; x < w; x++)
		{
			char r, g, b, a = 0;
			if (channels == 1)
			{
				r = data[x * channels];
				g = data[x * channels];
				b = data[x * channels];
			}
			else if (channels == 3 || channels == 4)
			{
				r = data[x * channels + 2];
				g = data[x * channels + 1];
				b = data[x * channels];
			}

			if (channels == 4)
			{
				a = data[x * channels + 3];
				qimg->setPixel(x, y, qRgba(r, g, b, a));
			}
			else
			{
				qimg->setPixel(x, y, qRgb(r, g, b));
			}
		}
	}


}
void Scene::preview(){
	cvNamedWindow("Podglad", CV_WINDOW_AUTOSIZE);
	CvFont font;
	cvInitFont(&font, CV_FONT_HERSHEY_SCRIPT_SIMPLEX, 1, 1);
	IplImage *thumbnail = cvCreateImage(cvSize(800, 600), image->depth, image->nChannels);
	cvResize(image, thumbnail);

	char number[5];
	for(int i=0; i< positions.count(); i++)
	{
		sprintf(number, "%d", (i+1));
		CvPoint intPoint = cvPoint(cvRound(positions[i].x*(800./image->width)), cvRound(positions[i].y*(600./image->height)));
		cvPutText(thumbnail, number, intPoint, &font, cvScalar(0, 255, 0));
	}
	cvShowImage("Podglad", thumbnail);
	cvReleaseImage(&thumbnail);
}
//! Wykorzystuje algorytm Otsu do progowania, na wcześniej znalezionych obszarach (ROI)
void Scene::circleMarkers(QList<CvPoint2D32f> &positions, int markersDeclared)
{
	positions.clear();
	if(!image){
		QMessageBox::warning(NULL, "Blad", "Obraz do analizy niedostepny");
		return;
	}
	//znalezienie obszarów, gdzie znajdują się znaczniki
	QList<CvPoint2D32f> circles;
	QList<double> circlesRadius;
	circlesRadius = circlesApprox(circles, markersDeclared); //wykrywanie okregow

	//obrazy pośrednie
	grayImage = cvCreateImage(cvGetSize(image), 8, 1);
	Mat processedImage(cvGetSize(image), 8, 1);
	//Mat threshImage(cvGetSize(image), 8, 1);

	//przetworzenie na skalę szarości
	if(image->nChannels ==1)
		cvCopy(image, grayImage);
	else
		cvCvtColor(image, grayImage, CV_BGR2GRAY);
	GaussianBlur(cvarrToMat(grayImage), processedImage, Size(3, 3), 0);

	
	std::vector<std::vector<Point> > contours; //wyjściowa macierz dla findContours
	std::vector<Vec4i> contourHierarchy;
	int x, y, w,h;
	for(int i=0; i<circles.count(); i++)
	{
		contours.clear();
		x = cvRound(circles[i].x - 1.5*circlesRadius[i]); //obszar poszukiwania narożnika
		x = (x < 0 ? 0 : x);
		y = cvRound(circles[i].y - 1.5*circlesRadius[i]);
		y = (y < 0 ? 0 : y);
		w=h=3*cvRound(circlesRadius[i]);
		if(x+w >= image->width)
			w = image->width - x-1;
		if(y+h >= image->height)
			h= image->height - y-1;
		Mat roi(processedImage, Rect(x, y, w, h));
		//imshow("roi", roi);
		//waitKey();
		//int n = cvRound(circlesRadius[i]/2)*2+1;
		//adaptiveThreshold(processedImage, threshImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, n);
		int usedT; //wartość progu obliczona przez algorytm Otsu
		usedT = threshold(roi, roi, 128, 255, THRESH_BINARY | THRESH_OTSU);
		
		findContours(roi, contours, contourHierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE);
		

		//wyszukiwanie największego segmentu
		double maxArea=0;
		int biggestContId=0;
		double currentArea;
		for(int i=0; i < contours.size(); i++)
		{
			if(contourHierarchy[i][2] > 0)
				continue; //obszar nie jest "otworem"
			currentArea=contourArea(contours[i]);
			if(currentArea > maxArea)
			{
				maxArea = currentArea;
				biggestContId=i;
			}
		}
		//imshow("roi", roi);
		//waitKey();
		if(contours.size() == 0)
			continue;
		RotatedRect boundingRect; //prostokąt, w który wpisana jest elipsa markera
		boundingRect = fitEllipse(contours[biggestContId]);
		if(boundingRect.size.width*boundingRect.size.height < 25) //minimalny rozmiar to 5x5
			continue;
		Point2d currentPosition = boundingRect.center;
		currentPosition.x += x; //dodanie polozenia ROI
		currentPosition.y += y;
		positions.push_back(currentPosition);
	}
}
